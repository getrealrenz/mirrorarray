package com.company;

import java.util.Scanner;

/**
 * Created by ����� on 02.02.2016.
 */
public class Printer {
   // private int arr[][]={{2,1,0,9,5},{2,4,1,3,9},{4,4,5,1,0},{9,7,4,4,1},{0,9,4,2,2}};
    int N=5;
private int arr[][];

    public void initArray() {
        System.out.println("Enter number of rows and columns");
        Scanner sc=new Scanner(System.in);
        N=sc.nextInt();
        arr = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                arr[i][j] = ((int) (Math.random() * 9));
            }
        }

    }
    public void printArray()
    {


        for ( int i = 0; i < N; ++i ){
            for ( int j = 0; j < N-i ; ++j )
                System.out.print(" "+arr[i][j]);
            System.out.println();
        }
    }
    public void printMirror()
    {
        int j;
        for ( int i = 0; i < N; ++i ){
            for (  j = 0; j <N-i-1; ++j )
                System.out.print("  ");
            while (j<N)
                System.out.print(" "+arr[i][j++]);
            System.out.println();
        }
    }

}

